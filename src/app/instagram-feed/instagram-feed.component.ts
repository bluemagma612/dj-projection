import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {InstagramFeedService} from "../services/instagram-feed-service.service";

@Component({
	selector: 'instagram-feed',
	templateUrl: './instagram-feed.component.html',
	styleUrls: ['./instagram-feed.component.css']
})
export class InstagramFeedComponent implements OnInit {

	photoSrc: String;
	height: String = '640';
	width: String = '640';
	caption: String;
	username: String;
	likes: Number;
	tags: String;
	newPhotos: String;

	node_server: String = 'http://' + window.location.hostname + ':4200/';

	constructor(private http: HttpClient,
			  private _instagramService: InstagramFeedService) {
	}

	ngOnInit(): void {
		this._instagramService.getLatestPhoto(this.node_server)
			.subscribe(response => {
				// const info = response;
				this.newPhotos = response['info'];
			});

		this._instagramService.getRandomPhoto(this.node_server)
			.subscribe(response => {
				const data = response['data'];
				this.photoSrc = this.node_server + data.local_file_name;
				this.caption = data.caption;
				this.username = data.username;
				this.likes = data.likes;
				this.tags = data.tags;
			});
	}
}
