export interface MusicData {
	album;
	artist;
	title;
	albumart;
	status;
}
