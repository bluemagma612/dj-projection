import { TestBed, inject } from '@angular/core/testing';

import { MusicFeedServiceService } from './music-feed-service.service';

describe('MusicFeedServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MusicFeedServiceService]
    });
  });

  it('should be created', inject([MusicFeedServiceService], (service: MusicFeedServiceService) => {
    expect(service).toBeTruthy();
  }));
});
