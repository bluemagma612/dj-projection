import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class MusicFeedService {

	constructor(private http: HttpClient){
	}

	getCurrentMusic = (volumioServer) => {
		return Observable
			.interval(5000)
			.flatMap((i) => this.http.get(volumioServer + '/api/v1/getstate'));
		}
}
