import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class InstagramFeedService {
	constructor(private http: HttpClient) {
	}

	getLatestPhoto = (node_server) => {
		return Observable
			.interval(30000)
			.flatMap((i) => this.http.get(node_server + 'api/instagram-photos/latest'));
	}

	getRandomPhoto = (node_server) => {
		return this.http.get(node_server + 'api/instagram-photos/random');
	}
}
