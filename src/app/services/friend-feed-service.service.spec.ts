import { TestBed, inject } from '@angular/core/testing';

import { FriendFeedServiceService } from './friend-feed-service.service';

describe('FriendFeedServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FriendFeedServiceService]
    });
  });

  it('should be created', inject([FriendFeedServiceService], (service: FriendFeedServiceService) => {
    expect(service).toBeTruthy();
  }));
});
