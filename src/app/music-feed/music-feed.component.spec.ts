import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MusicFeedComponent } from './music-feed.component';

describe('MusicFeedComponent', () => {
  let component: MusicFeedComponent;
  let fixture: ComponentFixture<MusicFeedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MusicFeedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MusicFeedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
