import { Component, OnInit } from '@angular/core';
import { MusicFeedService } from '../services/music-feed-service.service';
import { MusicData } from '../music-data';

@Component({
  selector: 'music-feed',
  templateUrl: './music-feed.component.html',
  styleUrls: ['./music-feed.component.css']
})

export class MusicFeedComponent implements OnInit {

  	trackTitle;
  	albumTitle;
  	releaseDate;
	artistName;
	albumArt;

	volumioServer = 'http://volumio.local';

  constructor(private _musicFeedService: MusicFeedService) { }

  ngOnInit(): void {
	this._musicFeedService.getCurrentMusic(this.volumioServer)
		.subscribe((data: MusicData) => {
			if (data.status !== 'stop') {
				this.albumTitle = data.album;
				this.artistName = data.artist;
				this.trackTitle = data.title;
				this.albumArt = this.volumioServer.concat(data.albumart);
			}
		});
  }
}
