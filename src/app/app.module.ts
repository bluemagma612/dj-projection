import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {InstagramFeedComponent} from './instagram-feed/instagram-feed.component';
import {FriendFeedComponent} from './friend-feed/friend-feed.component';
import {MusicFeedComponent} from './music-feed/music-feed.component';
import {MusicFeedService} from './services/music-feed-service.service';
import {InstagramFeedService} from './services/instagram-feed-service.service';
import {HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
	declarations: [
		AppComponent,
		InstagramFeedComponent,
		FriendFeedComponent,
		MusicFeedComponent
	],
	imports: [
		BrowserModule,
		HttpClientModule,
		NgbModule.forRoot()
	],
	providers: [
		MusicFeedService,
		InstagramFeedService
	],
	bootstrap: [AppComponent]
})
export class AppModule {
}
