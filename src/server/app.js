var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

// add timestamps in front of log messages
require('console-stamp')(console, '[HH:MM:ss.l]');

var instagramPhotos = require('./routes/instagram-photos');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/api/instagram-photos', instagramPhotos);

app.use(express.static('instagram-photos'));

module.exports = app;
