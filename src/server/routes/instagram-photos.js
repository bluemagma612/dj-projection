const express = require('express');
const router = express.Router();
const Promise = require('bluebird');
const sqlite3 = require('sqlite3');
const path = require('path');
const axios = require('axios');
const fs = require('fs');

const dbPath = path.resolve(__dirname, '../database/instagram-photos.db');
const photoPath = path.resolve(__dirname, '../instagram-photos');
const db = new sqlite3.Database(dbPath);

// instagram access token
const accessToken = '2952606069.52cae3e.6fa2f133750342c5bbd8851ff7ffc6a9';

const url = 'https://api.instagram.com/v1/tags/blidahl2017/media/recent?access_token=' + accessToken;

var id,
	caption,
	uri,
	MIN_TAG_ID = '',
	db_MIN_TAG_ID;
//--------------------------------------------------------------------------------------------------------------------->
router.get('/random', function(req,res,next) {
	try {
		db.all('SELECT * FROM instagramPhotos ORDER BY RANDOM() LIMIT 1', function(err,rows) {
			res.setHeader('Access-Control-Allow-Origin', '*');
			res.json({data: rows[0]})
		});
	} catch (err) {
		res.send('error getting instagram photos ' + err);
	}
});
//--------------------------------------------------------------------------------------------------------------------->
router.get('/latest', function(req,res,next) {
	 getDbMinTagId()
		.then(function(db_min_tag_id) {
			return getLatest(db_min_tag_id)
		})
		.then(function(data) {
			console.log('vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv');
			console.log(' data = ', data );
			console.log('^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^');
			if (data.data.length != 0) {
				return saveNewPhoto(data);
			}
			return Promise.reject('no new photos');
		})
		.then(function(photoFile) {
			// download the photo to app folder
			return getPhoto(photoFile);
		})
		 .then(function(response) {
			res.setHeader('Access-Control-Allow-Origin', '*');
		 	res.json({info: response});
		 })
		.catch(function(error) {
			console.log(error);
			res.setHeader('Access-Control-Allow-Origin', '*');
			res.json({info: error});
		});
	//---------------------------------------------------------------------------------------------------------------->
	function getDbMinTagId() {
		return new Promise(function(resolve, reject) {
			db.all('SELECT min_tag_id FROM instagramPhotos ORDER BY created ASC LIMIT 1', function(err, rows) {
				if (err) {
					reject(err);
				} else {
					resolve(rows[0].min_tag_id);
				}
			})
		});
	}
	//---------------------------------------------------------------------------------------------------------------->
	function saveNewPhoto(data) {
		var photo = data.data[0];
		var tags = photo.tags.join();
		var url = photo.images.standard_resolution.url;
		var fileName = url.substring(url.lastIndexOf('/')+1);

		return new Promise(function(resolve, reject) {
			db.run('INSERT INTO instagramPhotos VALUES ("'+
				data.pagination.min_tag_id + '","' +
				photo.id + '",' +
				photo.created_time + ',"' +
				photo.caption.text + '","' +
				photo.images.standard_resolution.url + '","' +
				fileName + '","' +
				photo.user.username + '",' +
				photo.likes.count + ',"' +
				tags + '")', function(err, rows) {
				if (err) {
					var responseObj = {
						'error': err
					};
					reject(responseObj);
				} else {
					var responseObj = {
						url: photo.images.standard_resolution.url,
						fileName: fileName
					};
					resolve(responseObj);
				}
			});
		});
	}
	//---------------------------------------------------------------------------------------------------------------->
	function getLatest(db_MIN_TAG_ID) {
		return axios
			.get(url + '&max_id=' + db_MIN_TAG_ID )
			.then(function (response) {
				console.log('vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv');
				console.log(' url = ', url + '&max_id=' + db_MIN_TAG_ID );
				console.log('^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^');
				return response.data;
			})
			.catch(function (error) {
				console.log('error ', error);
				res.json({'INFO': 'ERROR PULLING NEW INSTAGRAM PHOTOS ' + error})
			});
	}
	//---------------------------------------------------------------------------------------------------------------->
	function getPhoto(photoFile) {
		return axios({
			method: 'get',
			url: photoFile.url,
			responseType: 'stream'
		})
		.then(function(response) {
			response.data.pipe(fs.createWriteStream(photoPath + '/' + photoFile.fileName));
			return 'success';
		});
	}

});

module.exports = router;
